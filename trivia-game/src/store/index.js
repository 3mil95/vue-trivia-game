import Vue from 'vue'
import Vuex from 'vuex'
import { questions } from '../utils/mockdata/questions';
import { getQuestions } from '../utils/api.js'
import { formatQuestion } from '../utils/questionFormatting.js'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    questions: [],
    answers: [],
    quizSettings: {},
    loading: false,
    error: "",
  },
  mutations: {
    // Sets the quiz settings.
    setQuizSettings: (state, payload) => {
      state.quizSettings = payload;
    },
    // Sets questions to payload.
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    // Sets answers to payload. 
    setAnswers: (state, payload) => {
      state.answers = payload;
    },
    // Sets the answer att index payload.index to payload.answer.
    setAnswer: (state, payload) => {
      const newAnswers = [...state.answers];
      newAnswers[payload.index-1] = payload.answer;
      state.answers = newAnswers;
    },
    // Sets error to payload.
    setError: (state, payload) => {
      state.error = payload;
    },
    // Sets loading to payload.
    setLoading: (state, payload) => {
      state.loading = payload;
    }
  },
  actions: {
    // Action to set a single answer to payload.answer at index payload.index.
    selectAnswers: ({ commit }, payload) => {
      commit('setAnswer', payload);
    },
    // Action to start a quiz by fetching questions with the previous settings.
    fetchNewQuestions: ({ getters, dispatch  }) => {
      dispatch('fetchQuestions', getters.getQuizSettings); 
    },
    // Action to start a quiz by fetching questions with params in payload.
    fetchQuestions: async ({ commit }, payload) => {
      commit('setQuizSettings', payload);
      commit('setLoading', true);
      commit('setQuestions',  []);
      commit('setAnswers', []);
      commit('setError', "");
      let error;
      let questions = await getQuestions(payload.numberOfQuestions, payload.difficulty, payload.category).catch(err => error = `Error fetching questions: ${err.message}.`);
      
      if (error) {
        commit('setError', error);
        commit('setLoading', false);
        return;
      }

      commit('setQuestions', formatQuestion(questions.results));
      commit('setLoading', false);
    },
    // Action to start a quiz by fetching questions from mock data.
    fetchMockQuestions: ({ commit }) => {
      commit('setAnswers', []);
      commit('setQuestions',  formatQuestion(questions));
    }
  },
  getters: {

    // Gets the result of the quiz.
    getResult: (state) => {
      const { questions, answers } = state;
      let score = 0;
      const maxScore = questions.length * 10;
      for (let i = 0; i < questions.length; i++) {
        if (questions[i].correct_answer === answers[i]) {
          score += 10;
        }
        questions[i].selectedAnswer = answers[i];
      }

      return { score, maxScore, questions};
    },
    // Gets the quiz settings.
    getQuizSettings: (state) => state.quizSettings,
    // Gets a single question by index.
    getQuestion: (state) => (index) => state.questions[index-1],
    // Gats single answer by index.
    getAnswer: (state) => (index) => state.answers[index-1],
    // Gets the number of questions in the quiz
    getNumberOfQuestion: (state) => state.questions.length,
    // Get if state is loading.
    isLoading: (state) => state.loading,
    // Get state error.
    getError: (state) => state.error,
  }
})
