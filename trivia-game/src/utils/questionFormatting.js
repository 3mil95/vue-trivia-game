
import he from 'he';

// Formats the questions to work with the front-end.
export const formatQuestion = function(questions) {
    return questions.map(question => {
        const answers = [question.correct_answer, ...question.incorrect_answers].map(answer => he.decode(answer))
        shuffleAnswer(answers);
        question.correct_answer = he.decode(question.correct_answer);
        question.question = he.decode(question.question);
        question ["answers"] = answers;
        return question;
    })
}

// Shuffle the answers in the list sent in.
const shuffleAnswer = function(answers) {
    for (let i = 0; i < 10; i++) {
        const i1 = getRandomIndex(answers.length);
        const i2 = getRandomIndex(answers.length);

        const temp = answers[i2];
        answers[i2] = answers[i1]; 
        answers[i1] = temp; 
    }
}

// Returns a random index in range 0 to max.
const getRandomIndex = function(max) {
    return Math.floor(Math.random() * max);
}