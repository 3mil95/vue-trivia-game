
export const questions = [
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "In what year was the famous 45 foot tall Hollywood sign first erected?",
    "correct_answer": "1923",
    "incorrect_answers": [
        "1903",
        "1913",
        "1933"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "Where and when was the first cardboard box made for industrial use?",
    "correct_answer": "England, 1817",
    "incorrect_answers": [
        "United States, 1817",
        "England, 1917",
        "United States, 1917"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "What year were the Marian Reforms instituted in the Roman Republic?",
    "correct_answer": "107 BCE",
    "incorrect_answers": [
        "42 BCE",
        "264 BCE",
        "102 CE"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "Adolf Hitler was born on which date?",
    "correct_answer": "April 20, 1889",
    "incorrect_answers": [
        "June 12, 1889",
        "February 6, 1889",
        "April 16, 1889"
    ]
},
{
    "category": "History",
    "type": "boolean",
    "difficulty": "easy",
    "question": "The United States of America declared their independence from the British Empire on July 4th, 1776.",
    "correct_answer": "True",
    "incorrect_answers": [
        "False"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "hard",
    "question": "Who was the President of the United States during the signing of the Gadsden Purchase?",
    "correct_answer": "Franklin Pierce",
    "incorrect_answers": [
        "Andrew Johnson",
        "Abraham Lincoln",
        "James Polk"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "Who was the only US President to be elected four times?",
    "correct_answer": "Franklin Roosevelt",
    "incorrect_answers": [
        "Theodore Roosevelt",
        "George Washington",
        "Abraham Lincoln"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "The Korean War started in what year?",
    "correct_answer": "1950",
    "incorrect_answers": [
        "1945",
        "1960",
        "1912"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "The Thirty Years War ended with which treaty?",
    "correct_answer": "Peace of Westphalia",
    "incorrect_answers": [
        "Treaty of Versailles",
        "Treaty of Paris",
        "Peace of Prague"
    ]
},
{
    "category": "History",
    "type": "multiple",
    "difficulty": "medium",
    "question": "Which of these founding fathers of the United States of America later became president?",
    "correct_answer": "James Monroe",
    "incorrect_answers": [
        "Alexander Hamilton",
        "Samuel Adams",
        "Roger Sherman"
    ]
}
]