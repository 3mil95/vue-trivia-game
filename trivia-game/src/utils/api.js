
const BASE_URL = "https://opentdb.com"


const responseCodeMap = {
  1: "No Results Could not return results. The API doesn't have enough questions for your query. (Ex. Asking for 50 Questions in a Category that only has 20.)",
  2: "Invalid Parameter Contains an invalid parameter. Arguements passed in aren't valid. (Ex. Amount = Five)",
  3: "oken Not Found Session Token does not exist.",
  4: "Token Empty Session Token has returned all possible questions for the specified query. Resetting the Token is necessary."
}

// Fetches categories from api
export const getCategories = () => {
  return fetch(BASE_URL+'/api_category.php')
      .then((response) => response.json())
      .then(data => data.trivia_categories)
} 

// Fetches questions from api
export const getQuestions = (numberOfQuestions, difficulty=null, category=null) => {

    let url = BASE_URL + `/api.php?amount=${numberOfQuestions}`;
    url += (category) ? `&category=${category}` : "";
    url += (difficulty) ? `&difficulty=${difficulty}` : "";    

    return fetch(url)
        .then((response) => response.json())
        .then(data => {
          if (data.response_code === 0) {
            return data
          }
          throw new Error("Error: " + responseCodeMap[data.response_code])
        })
}
